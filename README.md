[TOC]
# sock-rpc Mission#

  * This is an Arma 3 mission, that implements the client-side for the Node.js sock-rpc module.
  * The mission uses the sock.dll extension to manage the underlying TCP/IP connection.

## Changelog ##
  * 0.0.5
    * Change underlying com protocol to use Raw SQF instead of JSON (improve performance), requires sock-rpc server v1.0.4 or higher
    * Enhancement: Add support for pseudo SQF hash data type using ```sock_hash``` function
  * 0.0.4
    * Defect fix: BOOLEAN values were being mapped to JSON string instead of JSON boolean
    * Enhancement: Add support for pseudo SQF hash data type using ```sock_hash``` function
  * 0.0.3
    * Defect fix: SQF NaN values not mapped to valid JSON
  * 0.0.2 
    * Add support for invoking *sock_rpc* from client side (request, and result passed around using ```publicVariableServer```, and ```publicVariableClient```)
    * Improve synchronization between client and server when loading the sock.sqf library (client will wait for server before proceeding)
  * 0.0.1 - Initial release


## Prerequisites ##
  * Install and configure the [sock-rpc](https://bitbucket.org/micovery/sock-rpc#markdown-header-sock-rpc) Node.js module
  * Install and configure the [sock.dll](https://bitbucket.org/micovery/sock.dll#markdown-header-sockdll) extension.


## Configuration ##
  * Download the [sock-rpc.Altis.pbo](https://bitbucket.org/micovery/sock-rpc.mission/raw/v0.0.2/Arma%203/MPMissions/sock-rpc.Altis.pbo) mission file
  * Copy the mission file to the Arma 3 MPMissions directory (i.e. C:\Program Files (x86)\Steam\steamapps\common\Arma 3\MPMissions)
  * Start the sock-rpc Node.js server
  * Start the game, and load the mission


## SQF API ##

  * sock_rpc(*method*, [*params*], [*default*])

    * *method* (type: string, required) - This is the name of the remote method to invoke  

        The method must have been registered in the Node.js sock-rpc server  
        .  

    * *params* (type: array, optional) - An array containing the parameters to be passed to the remote *method*  

        If not provided, it defaults to empty array []  
        .  

    * *default* (type: any, optional) - This is the value to return if the remote method invocation fails  

        If not provided, it defaults to nil  
        .  
    This function can be invoked either on client side, or server side. (*when invoked on client side, it must be within a scheduled environment*)

```javascript

    //Example for using the sock_rpc function on client side (notice the "spawn" call to force a scheduled environment).

    [] spawn {
        private["_method", "_response", "_params"];  
        _method = "getLocalTime";
        _params = ["PST"];

        _response = [_method, _params] call sock_rpc;

        if (isNil "_response") exitWith {
          player globalChat format["Error occurred while invoking the %1", _method];
        }
        else {
          player globalChat _response;
        };
    };
```
.  

  * sock_set_log_level(*level*)

    * *level* (type: number(0-6), required) - Sets the logging level for the sock.sqf client code.
        * 6 - LOG_SEVERE_LEVEL
        * 5 - LOG_WARNING_LEVEL
        * 4 - LOG_INFO_LEVEL
        * 3 - LOG_CONFIG_LEVEL;
        * 2 - LOG_FINE_LEVEL
        * 1 - LOG_FINER_LEVEL
        * 0 - LOG_FINEST_LEVEL  
        .  

```
    //Set default logging level for the sock-rpc client library

    LOG_INFO_LEVEL call sock_log_set_level;
```
.
  * sock_hash(*key-value-pairs*)
        .  
    This function is used for converting a set of key-value pairs into a hash representation that can be passed a argument into the ```sock_rpc``` function.  
    This is needed because SQF does not have a native hash data-type.  
    

```
    private["_hash"];
    _hash = [
              ["key1","value1"],
              ["key2",2],
              ["key3", ["a", "b", "c"]]
            ] call sock_hash;
```
.